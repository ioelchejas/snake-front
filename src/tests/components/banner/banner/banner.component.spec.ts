import { ComponentFixture, ComponentFixtureAutoDetect, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { BannerComponent } from './banner.component';
import { of, throwError } from 'rxjs';
import { BannerService } from './banner.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

fdescribe('BannerComponent', () => {
  let component: BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;
  let h1: HTMLElement;

  let testQuote = '';

  let getQuoteSpy: jasmine.SpyObj<any>;

  beforeEach(async () => {
    testQuote = 'Test Quote';
    const bannerService = jasmine.createSpyObj('BannerService', ['getStatus']);
    getQuoteSpy = bannerService.getStatus.and.returnValue(throwError(() => new Error('TwainService test failure')));

    await TestBed.configureTestingModule({
      imports: [BannerComponent, FormsModule],
      providers: [
        //{ provide: ComponentFixtureAutoDetect, useValue: true }
      ]
    }).overrideComponent(BannerComponent, {
      set: {
        providers: [{ provide: BannerService, useValue: bannerService }],
      }
    }).compileComponents();
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    h1 = fixture.nativeElement.querySelector('h1');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a different test title', () => {
    component.title = 'Test Title';
    fixture.detectChanges();
    expect(h1.textContent).toContain('Test Title');
  });

  it('should change title from input', () => {
    // get the name's input and display elements from the DOM
    const nameInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('input')!;
    const nameDisplay: HTMLElement = fixture.debugElement.nativeElement.querySelector('h1')!;

    // simulate user entering a new name into the input box
    nameInput.value = 'aaa';

    // Dispatch a DOM event so that Angular learns of input value change.
    nameInput.dispatchEvent(new Event('input'));

    // Tell Angular to update the display binding through the title pipe
    fixture.detectChanges();

    expect(nameDisplay.textContent).toBe('aaa');
  });

  it('should show quote after component initialized', () => {
    fixture.detectChanges();  // onInit()

    // sync spy result shows testQuote immediately after init
    expect(fixture.debugElement.nativeElement.querySelector('.twain').textContent).toBe(testQuote);
    expect(getQuoteSpy.calls.any())
      .withContext('getQuote called')
      .toBe(true);
  });

  fit('should display error when TwainService fails', fakeAsync(() => {
    // tell spy to return an error observable
    //getQuoteSpy.and.returnValue(throwError(() => new Error('TwainService test failure')));
    fixture.detectChanges();  // onInit()
    // sync spy errors immediately after init

    tick();  // flush the component's setTimeout()

    fixture.detectChanges();  // update errorMessage within setTimeout()

    expect(component.errorMessage)
      .withContext('should display error')
      .toMatch(/test failure/,);
    expect(fixture.debugElement.nativeElement.querySelector('.twain').textContent)
      .withContext('should show placeholder')
      .toBe('...');
  }));
});
