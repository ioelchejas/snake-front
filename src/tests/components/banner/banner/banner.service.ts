import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

@Injectable()
export class BannerService {
    getStatus(): Observable<string> {
        return of("online");
    }
}
