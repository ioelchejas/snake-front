import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { catchError, from, startWith, Observable, of, tap } from 'rxjs';
import { BannerService } from './banner.service';

@Component({
  selector: 'app-banner',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  providers: [BannerService]
})
export class BannerComponent implements OnInit {
  title = 'Test Tour of Heroes';


  constructor(private bannerService: BannerService) {

  }

  ngOnInit(): void {
    this.getQuote()
  }
  errorMessage = 'aaa';
  quote: Observable<any>
  getQuote() {
    // this.errorMessage = 'aaa';
    this.quote = this.bannerService.getStatus().pipe(
      startWith('...'),
      tap(() => {
        throw new Error("custom error")

      }),
      catchError((err: any) => {
        console.log(err.message)
        // Wait a turn because errorMessage already set once this turn
        setTimeout(() => { this.errorMessage = err.message || err.toString() });
        return of('...'); // reset message to placeholder
      })
    );
  }


}
