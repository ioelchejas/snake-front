import { Injectable } from '@angular/core';
import { from } from 'rxjs';

@Injectable()
export class ValueService {

  constructor() { }

  getValue() {
    return "real value";
  }

  getObservableValue() {
    return from(['observable value']);
  }

  getPromiseValue() {
    return new Promise((resolve) => {
      resolve('promise value')
    })
  }
}

@Injectable()
export class MasterService {
  constructor(private valueService: ValueService) { }
  getValue() { return this.valueService.getValue(); }
}
