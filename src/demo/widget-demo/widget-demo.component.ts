import { Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { statisticData } from './data';
import { WidgetComponent } from '../widget/widget.component';

@Component({
  selector: 'app-widget-demo',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './widget-demo.component.html',
  styleUrls: ['./widget-demo.component.scss']
})
export class WidgetDemoComponent {
  optionsByType = (type: string) => {
    switch (type) {
      case 'fb':
        return {
          component: () => import('../widget/widget.component').then(m => m.WidgetComponent),
          inputs: statisticData.find(item => item['id'] === type)!
        }
      case 'tw':
        return {
          component: () => import('../widget/widget.component').then(m => m.WidgetComponent),
          inputs: statisticData.find(item => item['id'] === type)!
        }
      case 'ig':
        return {
          component: () => import('../widget/widget.component').then(m => m.WidgetComponent),
          inputs: statisticData.find(item => item['id'] === type)!
        }
      default:
        return {
          component: () => import('../widget/widget.component').then(m => m.WidgetComponent),
          inputs: statisticData.find(item => item['id'] === type)!
        }
    }
  }

  @ViewChild('container', {
    static: true,
    read: ViewContainerRef
  }) container!: ViewContainerRef;
  buttons: any[] = [
    {
      label: 'Create Widget Facebook',
      action: () => this.createDynamicWidget('fb')
    },
    {
      label: 'Create Widget Twitter',
      action: () => this.createDynamicWidget('tw')
    },
    {
      label: 'Create Widget Instagram',
      action: () => this.createDynamicWidget('ig')
    }
  ]

  async createDynamicWidget(type: string) {

    this.container.clear();
    const { component, inputs } = this.optionsByType(type);

    const componentInstance = await component();
    const componentRef: ComponentRef<WidgetComponent> = this.container.createComponent(componentInstance);
    componentRef.instance.configOptions = inputs;
  }
}
