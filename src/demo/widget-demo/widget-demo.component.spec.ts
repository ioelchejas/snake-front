import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetDemoComponent } from './widget-demo.component';

describe('WidgetDemoComponent', () => {
  let component: WidgetDemoComponent;
  let fixture: ComponentFixture<WidgetDemoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [WidgetDemoComponent]
    });
    fixture = TestBed.createComponent(WidgetDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
