import { APP_INITIALIZER, enableProdMode, importProvidersFrom } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


import { environment } from './environments/environment';
import { AppComponent } from './app/app.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { MatLegacySelectModule as MatSelectModule } from '@angular/material/legacy-select';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyProgressBarModule as MatProgressBarModule } from '@angular/material/legacy-progress-bar';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatLegacyTableModule as MatTableModule } from '@angular/material/legacy-table';
import { provideAnimations } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app/app-routing.module';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { ExampleInterceptor } from './app/interceptors/interceptor';
import { HTTP_INTERCEPTORS, withInterceptorsFromDi, provideHttpClient } from '@angular/common/http';
import { EnvServiceProvider } from './app/services/env/env.service.provider';
import { CookieService } from 'ng2-cookies';
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from './app/services/auth/auth.service';

const config: SocketIoConfig = {
  url: 'wss://snake-back-8hio.onrender.com:8443', options: {
    withCredentials: true,
    transports: ['websocket', 'polling'],
  }
};

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(BrowserModule, AppRoutingModule, ReactiveFormsModule, FormsModule, MatTableModule, MatButtonModule, MatInputModule, MatCardModule, MatProgressBarModule, MatIconModule, MatSelectModule, SocketIoModule.forRoot(config)),
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    CookieService,
    EnvServiceProvider,
    { provide: HTTP_INTERCEPTORS, useClass: ExampleInterceptor, multi: true },
    provideHttpClient(withInterceptorsFromDi()),
    provideAnimations(),
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: authFactory,
    //   multi: true,
    //   deps: [AuthService],
    // }
  ]
})
  .catch(err => console.error(err));
