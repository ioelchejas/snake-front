import { AfterViewInit, Directive, ElementRef, HostListener, Input, NgZone } from '@angular/core';
import { Subscription } from 'rxjs';

@Directive({
    selector: '[skRipple]',
    standalone: true,
})
export class RippleDirective implements AfterViewInit {
  @Input() centerRipple = false;
  @Input() disableRipple = false;
  constructor(private el: ElementRef, private ngZone: NgZone) { }
ve
  rippleWidth: number;
  rippleHeight: number;
  XPosition: number;
  YPosition: number;

  onStableSubscription: Subscription;
  ngAfterViewInit(): void {
    this.onStableSubscription = this.ngZone.onStable.subscribe(() => {
      this.createAnimations();
      this.onStableSubscription.unsubscribe();
    })
    this.listenOnPointerDown();
  }

  private listenOnPointerDown(): void {
    this.el.nativeElement.addEventListener('pointerdown', (e: PointerEvent) => {
      if(!this.disableRipple){
        this.startAnimation(e.clientX, e.clientY);
      }
    });
  }

  @HostListener('mouseout')
  onMouseout(): void {
    this.speedAnimation();
  }

  @HostListener('window:mouseup')
  onWindowMouseup(): void {
    this.speedAnimation();
  }

  @HostListener('touchmove', ['$event'])
  onTouchMove(e: TouchEvent): void {
    let x = e.touches[0].clientX - window.pageXOffset;
    let y = e.touches[0].clientY - window.pageYOffset;
    var target = document.elementFromPoint(x, y);
    if (target !== this.el.nativeElement) {
      this.speedAnimation();
    }
  }

  @HostListener('window:touchend')
  onWindowTouchend(): void {
    this.speedAnimation();
  }

  private startAnimation(xPosition: number, yPosition: number): void {
    if (this.el.nativeElement.querySelector('.ii-ripple')) {
      this.el.nativeElement.querySelector('.ii-ripple').remove();
    }
    this.XPosition = xPosition - this.el.nativeElement.getBoundingClientRect().x//e.target.offsetLeft;
    this.YPosition = yPosition - this.el.nativeElement.getBoundingClientRect().y//e.target.offsetTop;
    let ripple = document.createElement('span');
    ripple.classList.add('ii-ripple');
    this.setAnimationStartPosition(ripple);
    ripple.style.cssText += 'background: #fff!important;position: absolute;background: #fff;transform: translate(-50%, -50%);pointer-events: none;border-radius: 50%;animation: slowRipple 10s linear infinite;'
    this.el.nativeElement.appendChild(ripple)
    setTimeout(() => {
      ripple.remove();
    }, 10000);
  }

  private speedAnimation(): void {
    if (this.el.nativeElement.querySelector('.ii-ripple')) {
      let ripple = this.el.nativeElement.querySelector('.ii-ripple');
      this.rippleWidth = ripple.offsetWidth;
      this.rippleHeight = ripple.offsetHeight;
      ripple.style.cssText += `height: ${this.rippleHeight}px; width: ${this.rippleWidth}px;`
      this.setAnimationStartPosition(ripple);
      ripple.style.cssText += 'background: #fff!important;position: absolute;background: #fff;transform: translate(-50%, -50%);pointer-events: none;border-radius: 50%;animation: fastRipple 1s linear infinite;'
      setTimeout(() => {
        ripple.remove();
      }, 1000);
    }
  }


  private createAnimations(): void {
    const keyFrames = document.createElement("style");
    const width = this.el.nativeElement.clientWidth;

    keyFrames.innerHTML = `
    @keyframes slowRipple {
      0% {
          width: 0px;
          height: 0px;
          opacity: 0.5;
      }
      50% {
          opacity: 0.2;
      }
      100% {
          width: ${width * 3}px;
          height: ${width * 3}px;
          opacity: 0;
      }
    }
    @keyframes fastRipple {
      0% {
          opacity: 0.5;
      }
      50% {
          opacity: 0.2;
      }
      100% {
          width: ${width * 3}px;
          height: ${width * 3}px;
          opacity: 0;
      }
    }
    `;

    this.el.nativeElement.appendChild(keyFrames);
  }

  private setAnimationStartPosition(ripple: HTMLSpanElement) {
    ripple.style.left = this.centerRipple ? '50%' : this.XPosition + 'px';
    ripple.style.top = this.centerRipple ? '50%' : this.YPosition + 'px';
  }

}

