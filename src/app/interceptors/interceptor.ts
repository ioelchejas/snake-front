import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { EnvService } from '../services/env/env.service';

@Injectable()
export class ExampleInterceptor implements HttpInterceptor {

  constructor(private env: EnvService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = this.env.backendUrl;
    console.log("intercept: ", this.env, url)
    req = req.clone({
      url: url + req.url
    });
    return next.handle(req);
  }
}