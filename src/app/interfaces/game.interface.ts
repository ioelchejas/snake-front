export interface CellI {
    isFood: boolean;
    isSnake: boolean;
    snakeDirection?: string;
    snakeFirst?: string;
    snakeLast?: string;
    snakeId?: string;
}

export type Direction = 'ArrowUp' | 'ArrowDown' | 'ArrowLeft' | 'ArrowRight';

export interface SnakeI {
    snakeId: string;
    cells: number[];
  }

