export type OnlineEvent = {
    userId: string;
}

export type OnlineEventRequestType =
    | 'JOIN'
    | 'LEAVE'
    | 'LEFT'
    | 'RIGHT'
    | 'UP'
    | 'DOWN'
    | 'LOOSE';

export type OnlineEventResponseType =
    | 'CANT_JOIN_ROOM_COMPLETE'
    | 'USER_JOIN_TO_GAME'
    | 'USER_LEAVE_GAME'
    | 'START'
    | 'LEFT'
    | 'RIGHT'
    | 'UP'
    | 'DOWN'
    | 'LOOSE';

export interface OnlineEventRequest extends OnlineEvent {
    gameEvent: OnlineEventRequestType;
    room: string;
};

export interface OnlineEventResponse extends OnlineEvent {
    gameEvent: OnlineEventResponseType;
}
