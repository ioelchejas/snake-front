import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, AfterViewInit } from '@angular/core';
import { UntypedFormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { interval, merge, Subject, Subscription, fromEvent } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { UserI } from 'src/app/services/users/interfaces/user.interface';
import { SnakeOnlineGameService } from './services/snake-online-game.service';
import { OnlineEventRequestType } from '../../interfaces/online-game-events';
import { OnlineGameService } from '../../services/game/game-online.service';
import { TitleComponent } from '../../components/title/title.component';
import { NgIf, NgFor, NgClass, AsyncPipe } from '@angular/common';

type Direction = 'ArrowUp' | 'ArrowDown' | 'ArrowLeft' | 'ArrowRight';

interface CellI {
  isFood: boolean;
  isSnake: boolean;
  snakeDirection?: string;
  snakeFirst?: string;
  snakeLast?: string;
  snakeId?: string;
}

interface SnakeI {
  snakeId: string;
  cells: number[];
}

@Component({
    selector: 'app-snake-online-game',
    templateUrl: './snake-online-game.component.html',
    styleUrls: ['./snake-online-game.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [OnlineGameService],
    standalone: true,
    imports: [FormsModule, ReactiveFormsModule, NgIf, TitleComponent, NgFor, NgClass, AsyncPipe]
})
export class SnakeOnlineGameComponent implements AfterViewInit {

  form = this.fb.group({
    room: [null]
  })

  connect(): void {
    this.snakeOnlineGameService.leaveRoom();
    this.snakeOnlineGameService.joinRoom(this.form.controls.room.value);
  }

  cells$;
  playing$;
  score$;

  constructor(
    private fb: UntypedFormBuilder,
    private snakeOnlineGameService: SnakeOnlineGameService,
    private onlineGameService: OnlineGameService
  ) {
    this.cells$ = this.onlineGameService.cells$;
    this.playing$ = this.onlineGameService.playing$;
    this.score$ = this.onlineGameService.score$;
  }
  ngAfterViewInit(): void {
    // setInterval(function () {
    //   console.log("HOLA")
    //   postMessage('', "*");
    // }, 200);

    // var blob = new Blob([document.querySelector('#worker-code').textContent]);
    // var worker = new Worker(window.URL.createObjectURL(blob));

    // worker.onmessage = function () {
    //   console.log("TIME")
    // }
  }

}
