import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { OnlineEventRequest, OnlineEventResponse } from 'src/app/interfaces/online-game-events';
import { AppStateService } from 'src/app/services/app-state.service';
import { OnlineEventRequestType } from '../../../interfaces/online-game-events';

@Injectable({
  providedIn: 'root'
})
export class SnakeOnlineGameService {
  private _onlineEvent$ = new Subject<OnlineEventResponse>();
  public onlineEvent$ = this._onlineEvent$.asObservable();

  private _movementEvent$ = new Subject<OnlineEventResponse>();
  public movementEvent$ = this._movementEvent$.asObservable();

  private _startGame$ = new Subject<string[]>();
  public startGame$ = this._startGame$.asObservable();

  private _leftGame$ = new Subject<string>();
  public leftGame$ = this._leftGame$.asObservable();

  private _room$ = new BehaviorSubject<string | null>(null);

  public snakeToUserId = this._onlineEvent$.asObservable();

  private subscriptions = new Subscription();

  private userId: string;

  constructor(
    private appState: AppStateService,
    private socket: Socket
  ) {
    this.subscriptions.add(
      this.appState.data$.subscribe(data => {
        this.userId = data.userId;
      })
    );

    socket.fromEvent<OnlineEventResponse>('USER_JOIN_TO_GAME').subscribe((event) => {
      this._onlineEvent$.next(event);
      console.log('USER_JOIN_TO_GAME', event)
    });

    socket.fromEvent<OnlineEventResponse>('CANT_JOIN_ROOM_COMPLETE').subscribe((event) => {
      this._onlineEvent$.next(event);
      console.log('CANT_JOIN_ROOM_COMPLETE', event)
    });

    socket.fromEvent<OnlineEventResponse>('GAME_EVENT').subscribe((event) => {
      this._movementEvent$.next(event);
    });

    socket.fromEvent<string[]>('START').subscribe((event) => {
      this._startGame$.next(event);
      console.log('start: ', event)
    });

    socket.fromEvent<string>('USER_LEFT_THE_GAME').subscribe((event) => {
      this._leftGame$.next(event);
      console.log('left the game: ', event)
    });

    socket.fromEvent('disconnect').subscribe(() => {
      // const lastRoom = this._room$.getValue();
      // if (lastRoom) this.joinRoom(lastRoom);
    });

    socket.fromEvent('close').subscribe(() => {
      console.log("CLOSE")
    });

    socket.fromEvent('error').subscribe(() => {
      console.log("ERROR")
    });
  }

  sendOnlineEvent(gameEvent: OnlineEventRequestType): void {
    const roomCurrent = this._room$.getValue();
    if (roomCurrent) {
      console.log(this.userId, " EMIT: ", gameEvent)
      this.socket.emit('GAME_EVENT', {
        userId: this.userId,
        gameEvent,
        room: roomCurrent
      });
    }
  }

  joinRoom(room: string): void {
    console.log("TRY TO JOIN ROOM: ", room)
    this._room$.next(room);
    this.socket.emit('EVENT_JOIN', {
      userId: this.userId,
      room
    });
  }

  leaveRoom(): void {
    const room = this._room$.getValue();
    this.socket.emit('EVENT_LEAVE', {
      gameEvent: 'LEAVE',
      room,
      userId: this.userId
    } as OnlineEventRequest);
  }

}
