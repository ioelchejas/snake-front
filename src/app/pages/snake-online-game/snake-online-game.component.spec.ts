import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SnakeOnlineGameComponent } from './snake-online-game.component';

describe('SnakeOnlineGameComponent', () => {
  let component: SnakeOnlineGameComponent;
  let fixture: ComponentFixture<SnakeOnlineGameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [SnakeOnlineGameComponent]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SnakeOnlineGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
