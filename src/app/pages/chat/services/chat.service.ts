import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private _chat$ = new BehaviorSubject<string[]>([]);
  public chat$ = this._chat$.asObservable();

  private _room$ = new BehaviorSubject<string | null>(null);

  constructor(private socket: Socket) {
    socket.fromEvent('join_room').subscribe((message: string) => {
      this.setChat(message)
      console.log(message)
    });

    socket.fromEvent('cant_join_room').subscribe((message: string) => {
      this.setChat(message)
      console.log(message)
    });

    socket.fromEvent('new_message').subscribe((message: string) => {
      this.setChat(message);
    });

    socket.fromEvent('disconnect').subscribe(() => {
      const lastRoom = this._room$.getValue();
      if (lastRoom) this.joinRoom(lastRoom);
    });
  }

  public setChat(message: string): void {
    const current = this._chat$.getValue();
    const state = [...current, message];
    this._chat$.next(state);
  }

  public initChat(): void {
    this._chat$.next([]);
  }

  //TODO Enviar mensaje desde el FRONT-> BACKEND
  sendMessage(message: string) {
    const roomCurrent = this._room$.getValue();
    if (roomCurrent) {
      const payload = { message: message, room: roomCurrent };
      console.log(payload);
      this.socket.emit('event_message', payload);
    }
  }

  joinRoom(room: string): void {
    console.log("ROOM: ", room)
    this._room$.next(room);
    this.socket.emit('event_join', room);
  }

  leaveRoom(): void {
    const room = this._room$.getValue();
    this.socket.emit('event_leave', room);
  }

}
