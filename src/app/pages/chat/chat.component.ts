import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, AfterViewInit } from '@angular/core';
import { UntypedFormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatService } from './services/chat.service';
import { NgFor } from '@angular/common';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [
        FormsModule,
        ReactiveFormsModule,
        NgFor,
    ],
})
export class ChatComponent implements OnInit, AfterViewInit {

  _disabled = false;

  messages: string[] = [];
  constructor(private cd: ChangeDetectorRef, private fb: UntypedFormBuilder, private chatService: ChatService) {

  }
  form = this.fb.group({
    msj: [null],
    room: [null]
  })
  ngAfterViewInit(): void {
    this.chatService.chat$.subscribe(value => {
      this.messages = value;
      this.cd.detectChanges();
    })
  }

  ngOnInit(): void {
  }

  onClick(): void {
    this.sendMessage();
  }

  msgGeneration(msg, from): void {
    this.messages.push(`${from} says: ${msg}`);
    this.cd.detectChanges();
  }


  // NEST JS

  connect(): void {
    this.chatService.leaveRoom();
    //this.chatService.initChat();
    this.chatService.joinRoom(this.form.controls.room.value);
  }

  sendMessage(): void {
    this.chatService.sendMessage(this.form.controls.msj.value);
  }

}
