import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { OfflineGameService } from 'src/app/services/game/game-offline.service';
import { RankingI } from 'src/app/services/users/interfaces/ranking.interface';
import { UsersService } from 'src/app/services/users/users.service';
import { MatLegacyTableModule } from '@angular/material/legacy-table';
import { MatLegacyCardModule } from '@angular/material/legacy-card';
import { ModalComponent as ModalComponent_1 } from '../../components/modal/modal.component';
import { TitleComponent } from '../../components/title/title.component';
import { MatLegacyOptionModule } from '@angular/material/legacy-core';
import { MatLegacySelectModule } from '@angular/material/legacy-select';
import { MatLegacyFormFieldModule } from '@angular/material/legacy-form-field';
import { MatLegacyButtonModule } from '@angular/material/legacy-button';
import { NgIf, NgFor, NgClass, AsyncPipe } from '@angular/common';
import { BlockLayoutComponent } from '../../components/block-layout/block-layout.component';

interface CellI {
  isFood: boolean;
  isSnake: boolean;
  snakeDirection?: string;
  snakeFirst?: string;
  snakeLast?: string;
}

const levels = {
  easy: { moveTimeMS: 200, points: 1 },
  medium: { moveTimeMS: 150, points: 2 },
  hard: { moveTimeMS: 75, points: 4 },
  imposible: { moveTimeMS: 50, points: 8 },
}

@Component({
    selector: 'app-snake-game',
    templateUrl: './snake-game.component.html',
    styleUrls: ['./snake-game.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [OfflineGameService],
    standalone: true,
    imports: [BlockLayoutComponent, NgIf, MatLegacyButtonModule, MatLegacyFormFieldModule, MatLegacySelectModule, NgFor, MatLegacyOptionModule, TitleComponent, NgClass, ModalComponent_1, MatLegacyCardModule, MatLegacyTableModule, AsyncPipe]
})
export class SnakeGameComponent {
  @ViewChild('modal') modalComponent: ModalComponent;

  private level: keyof typeof levels = 'easy';
  readonly gameLevels = ['easy', 'medium', 'hard', 'imposible']


  // _maxUserScore: number = 0;
  _ranking: RankingI[];

  cells$;
  playing$;
  score$;

  constructor(
    private cd: ChangeDetectorRef,
    private usersService: UsersService,
    private offlineGameService: OfflineGameService
  ) {
    this.cells$ = this.offlineGameService.cells$;
    this.playing$ = this.offlineGameService.playing$;
    this.score$ = this.offlineGameService.score$;
    /*this.subscriptions.add(this.lostGame$.subscribe(() => {
      this._playing = false;
      this.usersService.updateRanking(this._score);
      const { score, _id } = (JSON.parse(localStorage.getItem('user')) as UserI);
      this._maxUserScore = score;
      if (this._score > score) {
        localStorage.setItem('user', JSON.stringify({ ...JSON.parse(localStorage.getItem('user')) as UserI, score: this._score }))
        this.usersService.setUserScore({ id: _id, score: this._score });
        this._maxUserScore = this._score;
      }
      this.cd.detectChanges();
      this.modalComponent.openModal();
    }));

    this.subscriptions.add(
      this.usersService.rankingResponse$.subscribe((res: RankingI[]) => {
        this._ranking = res;
        this.cd.detectChanges();
      })
    );*/
  }


  changeLevel(level: keyof typeof levels): void {
    this.level = level;
  }

  startGame(): void {
    this.offlineGameService.startGame([""]);
  }

  /*mobileControl(ev: KeyboardEvent): void {
    this.mobileKeys$.next(ev);
  }*/

}
