import { Component, ChangeDetectorRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingFullscreenService } from '../../components/loading-fullscreen/loading-fullscreen.service';
import { NotificationService } from '../../components/notification/notification.service';
import { AuthService } from '../../services/auth/auth.service';
import { CookieService } from '../../services/cookie/cookie.service';
import { MatLegacyProgressBarModule } from '@angular/material/legacy-progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyButtonModule } from '@angular/material/legacy-button';
import { NgIf } from '@angular/common';
import { MatLegacyInputModule } from '@angular/material/legacy-input';
import { MatLegacyFormFieldModule } from '@angular/material/legacy-form-field';
import { BlockLayoutComponent } from '../../components/block-layout/block-layout.component';
import { MatLegacyCardModule } from '@angular/material/legacy-card';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    standalone: true,
    imports: [FormsModule, ReactiveFormsModule, MatLegacyCardModule, BlockLayoutComponent, MatLegacyFormFieldModule, MatLegacyInputModule, NgIf, MatLegacyButtonModule, MatIconModule, MatLegacyProgressBarModule]
})
export class RegisterComponent {

  loading = false;
  hide = true;

  form: UntypedFormGroup = this.fb.group({
    email: [null, [Validators.required, Validators.email]],
    firstName: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(25)]],
    lastName: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(25)]],
    password: [null, [Validators.required, Validators.minLength(4)]],
  });
  constructor(private fb: UntypedFormBuilder, private authService: AuthService, private router: Router, private cookieService: CookieService, private notificationService: NotificationService, private cd: ChangeDetectorRef) {
    this.authService.registerSuccessResponse$.subscribe(() => {
      this.router.navigate(['app/snake-game']);
      this.notificationService.$showNotification.next({
        type: 'success',
        duration: 'medium',
        title: 'Cuenta creada con éxito!'
      });
    });
    this.authService.registerConflictResponse$.subscribe(() => {
      this.loading = false;
      this.cd.detectChanges();
      this.notificationService.$showNotification.next({
        type: 'error',
        duration: 'medium',
        title: 'Email en uso. Intente con otro email.'
      });
    });
    this.authService.internalServerError$.subscribe(() => {
      this.loading = false;
      this.cd.detectChanges();
      this.notificationService.$showNotification.next({
        type: 'alert',
        duration: 'medium',
        title: 'Servicio no disponible'
      });
    });
  }

  onRegisterClick(): void {
    if (this.form.valid) {
      this.loading = true;
      this.cd.detectChanges();
      this.authService.register({
        name: this.form.controls.firstName.value,
        lastName: this.form.controls.lastName.value,
        email: this.form.controls.email.value,
        password: this.form.controls.password.value
      });
    } else {
      this.notificationService.$showNotification.next({
        type: 'error',
        duration: 'medium',
        title: 'Los datos no son válidos'
      })
    }
  }

  goToLogin(): void {
    this.router.navigate(['login']);
  }
}
