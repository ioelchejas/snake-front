import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef, computed } from '@angular/core';
import { FormGroup, FormsModule, ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { NotificationService } from '../../components/notification/notification.service';
import { SelectComponent } from 'src/app/components/select/select/select.component';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { BlockLayoutComponent } from 'src/app/components/block-layout/block-layout.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';

import { NgIf } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { signal } from '@angular/core';
import { TextInputComponent } from 'src/app/new-components/text-input/text-input.component';
import { TextFieldComponent } from 'src/app/components/text-field/text-field.component';
import { BannerComponent } from "../../../tests/components/banner/banner/banner.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [TextFieldComponent, SelectComponent, MatCardModule, MatIconModule, MatFormFieldModule, FormsModule, ReactiveFormsModule, NgIf, MatInputModule, TextInputComponent, BannerComponent]
})
export class LoginComponent implements OnInit {
  loading = false;
  hide = true;

  // price = signal<number>(0);
  // priceWithDiscount = computed<number>(() => this.price() * 0.8);

  // onInputUpdate(event: Event): void {
  //   const newValue = +(event.target as HTMLInputElement).value;
  //   this.price.set(newValue);
  // }

  form: FormGroup = this.fb.group({
    text1: [{
      value: "",
      disabled: false
    }, [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4)]]
  });
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private notificationService: NotificationService, private cd: ChangeDetectorRef) { }
  ngOnInit(): void {
    this.form.controls.text1.valueChanges.subscribe((v) => {
      console.log(v)
    })
    if (this.authService.checkCredentials()) {
      this.router.navigate(['app/snake-game']);
    }
    this.authService.loginSuccessResponse$.subscribe(() => {
      this.router.navigate(['app/snake-game']);
      this.notificationService.$showNotification.next({
        type: 'success',
        duration: 'medium',
        title: 'Logeado con éxito!'
      });
    });
    this.authService.loginUnauthorizedResponse$.subscribe(() => {
      this.loading = false;
      this.cd.detectChanges();
      this.notificationService.$showNotification.next({
        type: 'error',
        duration: 'medium',
        title: 'Credenciales inválidas'
      });
    });
    this.authService.internalServerError$.subscribe(() => {
      this.loading = false;
      this.cd.detectChanges();
      this.notificationService.$showNotification.next({
        type: 'alert',
        duration: 'medium',
        title: 'Servicio no disponible'
      });
    });
  }

  onLoginClick(): void {
    if (this.form.valid) {
      this.loading = true;
      this.cd.detectChanges();
      this.authService.login({ email: this.form.controls.email.value, password: this.form.controls.password.value });
    } else {
      this.notificationService.$showNotification.next({
        type: 'error',
        duration: 'medium',
        title: 'Los datos no son válidos'
      });
    }
  }

  goToRegister(): void {
    this.router.navigate(['sign-in']);
  }

}
