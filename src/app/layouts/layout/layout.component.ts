import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { LateralMenuComponent } from '../../components/lateral-menu/lateral-menu.component';
import { Router, RouterOutlet } from '@angular/router';
import { CookieService } from 'ng2-cookies';
import { UserI } from 'src/app/services/users/interfaces/user.interface';
import { Subscription } from 'rxjs';
import { LateralMenuItemComponent } from '../../components/lateral-menu-item/lateral-menu-item.component';
import { BlockLayoutComponent } from '../../components/block-layout/block-layout.component';
import { LayoutContainerComponent } from '../../components/layout-container/layout-container.component';
import { TitleComponent } from '../../components/title/title.component';
import { ButtonIconComponent } from '../../components/button-icon/button-icon.component';
import { HeaderComponent } from '../../components/header/header.component';
import { AppContainerComponent } from '../../components/app-container/app-container.component';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    standalone: true,
    imports: [AppContainerComponent, HeaderComponent, ButtonIconComponent, TitleComponent, LayoutContainerComponent, RouterOutlet, LateralMenuComponent, BlockLayoutComponent, LateralMenuItemComponent]
})
export class LayoutComponent implements OnInit, OnDestroy {

  @ViewChild('lateralMenu') lateralMenu: LateralMenuComponent;

  mapaBitsActive = true;
  echotestActive = false;
  logActive = false;

  username = '';

  openLateralMenu = false;

  private subscriptions = new Subscription();

  constructor(private _service: AuthService, private router: Router, private cookieService: CookieService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    if (localStorage.getItem('user')) {
      const { name, lastName } = JSON.parse(localStorage.getItem('user')) as UserI;
      this.username = `${name} ${lastName}`;
    }
    this.subscriptions.add(this._service.userData$.subscribe((user: UserI) => {
      console.log("USER: ", user)
      localStorage.setItem('user', JSON.stringify(user))
      const { name, lastName } = user;
      this.username = `${name} ${lastName}`;
    }))
  }

  logout() {
    this._service.logout();
    this.router.navigate(['/', 'login']);
  }

  manageMenu(): void {
    this.lateralMenu.openMenu();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
