import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { NgClass } from '@angular/common';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

const types = {
  primary: [''],
  secondary: ['sk-button--secondary'],
};

@Component({
    selector: 'sk-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective, NgClass]
})
export class ButtonComponent implements OnInit {
  _type = '';
  @Input() set type(value: keyof typeof types) {
    this._type = (types[value] || []).join(' ');
  }

  _rounded = false;
  @Input() set rounded(value: boolean) {
    this._rounded = value;
  }

  _bottom = false;
  @Input() set bottom(value: boolean) {
    this._bottom = value;
  }

  _disabled: boolean = false;
  @Input() set disabled(value: any) {
    this._disabled = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
