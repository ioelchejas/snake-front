import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
    selector: 'sk-flex-layout',
    templateUrl: './flex-layout.component.html',
    styleUrls: ['./flex-layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass]
})
export class FlexLayoutComponent {
  @Input() centerVertical = false;
}
