import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NgClass } from '@angular/common';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

@Component({
    selector: 'sk-tab',
    templateUrl: './tab.component.html',
    styleUrls: ['./tab.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective, NgClass]
})
export class TabComponent {
  _active: boolean = false;
  @Input() set active(value: boolean) {
    this._active = value;
  }

}
