import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'sk-label',
    templateUrl: './label.component.html',
    styleUrls: ['./label.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class LabelComponent {

}
