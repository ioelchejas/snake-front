import { ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'sk-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class TabsComponent{

}
