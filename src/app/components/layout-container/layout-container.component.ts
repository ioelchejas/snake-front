import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'sk-layout-container',
    templateUrl: './layout-container.component.html',
    styleUrls: ['./layout-container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class LayoutContainerComponent {

}
