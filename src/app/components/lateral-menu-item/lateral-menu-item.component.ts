import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

@Component({
    selector: 'sk-lateral-menu-item',
    templateUrl: './lateral-menu-item.component.html',
    styleUrls: ['./lateral-menu-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective]
})
export class LateralMenuItemComponent {

}
