import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LateralMenuItemComponent } from './lateral-menu-item.component';

describe('LateralMenuItemComponent', () => {
  let component: LateralMenuItemComponent;
  let fixture: ComponentFixture<LateralMenuItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [LateralMenuItemComponent]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LateralMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
