import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'sk-stepper',
    templateUrl: './stepper.component.html',
    styleUrls: ['./stepper.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class StepperComponent implements OnInit {
  currentStep = 1;
  _width = '100%';

  @Input() steps = 1;


  @Input() set step(value: number) {
    this._width = `${(value * 100 / this.steps)}%`;
    this.currentStep = value;
  }

  ngOnInit(): void {
    this._width = `${(this.currentStep * 100 / this.steps)}%`;
  }

}
