import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NgSwitch, NgSwitchCase, NgTemplateOutlet } from '@angular/common';

const sizes = {
  xxlarge: ['sk-title--xxlarge'],
  xlarge: ['sk-title--xlarge'],
  large: ['sk-title--large'],
  medium: ['sk-title--medium'],
  small: ['sk-title--small'],
  xsmall: ['sk-title--xsmall'],
  xxsmall: ['sk-title--xxsmall'],
};

const aligns = {
  left: ['sk-title--left'],
  right: ['sk-title--right'],
  center: ['sk-title--center'],
};

const weights = {
  bold: ['sk-title--weight--bold'],
  medium: ['sk-title--weight--medium'],
};

@Component({
    selector: 'sk-title',
    templateUrl: './title.component.html',
    styleUrls: ['./title.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgSwitch, NgSwitchCase, NgTemplateOutlet]
})
export class TitleComponent {
  _align = 'sk-title--left';
  _weight = 'sk-title--weight--bold';
  _size = 'sk-title--medium';
  @Input() set align(value: keyof typeof aligns) {
    this._align = (aligns[value] || []).join(' ');
  }

  @Input() set weight(value: keyof typeof weights) {
    this._weight = (weights[value] || []).join(' ');
  }

  @Input() set size(value: keyof typeof sizes) {
    this._size = (sizes[value] || []).join(' ');
  }

}
