import { ChangeDetectionStrategy, Component} from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

@Component({
    selector: 'sk-list-item',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective, NgTemplateOutlet]
})
export class ListItemComponent {

}
