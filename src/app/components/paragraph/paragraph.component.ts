import { ChangeDetectionStrategy, Component, Input} from '@angular/core';
import { NgSwitch } from '@angular/common';

const sizes = {
  xxlarge: ['ft-paragraph--xxlarge'],
  xlarge: ['ft-paragraph--xlarge'],
  large: ['ft-paragraph--large'],
  medium: ['ft-paragraph--medium'],
  small: ['ft-paragraph--small'],
  xsmall: ['ft-paragraph--xsmall'],
  xxsmall: ['ft-paragraph--xxsmall'],
};

const aligns = {
  left: ['ft-paragraph--left'],
  right: ['ft-paragraph--right'],
  center: ['ft-paragraph--center'],
};

const weights = {
  bold: ['ft-paragraph--weight--bold'],
  medium: ['ft-paragraph--weight--medium'],
};

@Component({
    selector: 'sk-paragraph',
    templateUrl: './paragraph.component.html',
    styleUrls: ['./paragraph.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgSwitch]
})
export class ParagraphComponent {
  _align = 'ft-paragraph--left';
  _weight = 'ft-paragraph--weight--bold';
  _size = 'ft-paragraph--medium';
  @Input() set align(value: keyof typeof aligns) {
    this._align = (aligns[value] || []).join(' ');
  }

  @Input() set weight(value: keyof typeof weights) {
    this._weight = (weights[value] || []).join(' ');
  }

  @Input() set size(value: keyof typeof sizes) {
    this._size = (sizes[value] || []).join(' ');
  }

}
