import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'sk-bottom-container',
    templateUrl: './bottom-container.component.html',
    styleUrls: ['./bottom-container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class BottomContainerComponent {

}
