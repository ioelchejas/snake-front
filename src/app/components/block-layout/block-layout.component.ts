import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

const gaps = {
  xxlarge: ['sk-block-layout--gap-xxlarge'],
  xlarge: ['sk-block-layout--gap-xlarge'],
  large: ['sk-block-layout--gap-large'],
  medium: ['sk-block-layout--gap-medium'],
  small: ['sk-block-layout--gap-small'],
  xsmall: ['sk-block-layout--gap-xsmall'],
  xxsmall: ['sk-block-layout--gap-xxsmall'],
  none: ['sk-block-layout--gap-none'],
};

@Component({
    selector: 'sk-block-layout',
    templateUrl: './block-layout.component.html',
    styleUrls: ['./block-layout.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class BlockLayoutComponent {
  _gap = 'sk-block-layout--gap-medium';
  @Input() set gap(value: keyof typeof gaps) {
    this._gap = (gaps[value] || []).join(' ');
  }

  _top: string = '';
  @Input() set top(value: boolean | '') {
    this._top = value !== false ? 'sk-block-layout--top' : '';
  }

  _bottom: string = '';
  @Input() set bottom(value: boolean | '') {
    this._bottom = value !== false ? 'sk-block-layout--bottom' : '';
  }

}
