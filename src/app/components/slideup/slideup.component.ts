import { ChangeDetectionStrategy, Component, ElementRef, HostListener, Input, OnDestroy, Renderer2, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { NgClass, NgIf } from '@angular/common';

@Component({
    selector: 'ft-slideup',
    templateUrl: './slideup.component.html',
    styleUrls: ['./slideup.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass, NgIf]
})
export class SlideupComponent implements OnDestroy {
  _open: boolean = false;
  _showOverlay: boolean = false;
  constructor(private el: ElementRef, private _renderer: Renderer2, private cd: ChangeDetectorRef) { }

  _moving: boolean = false;
  yStartPosition: number = 0;

  onSliderTouchstart(e: TouchEvent): void {
    e.stopPropagation();
    this._moving = true;
    this.yStartPosition = e.touches[0].clientY;
    //this.listenOnMove(this.getXPositionOfTouchEvent(e.touches[0].clientX));
  }

  _scrollClass = '';
  @Input() set scrolleable(value: boolean) {
    this._scrollClass = value ? 'ft-slideup__content--scrolleable' : '';
  }

  @HostListener('window:touchmove', ['$event'])
  onWindowTouchmove(e: TouchEvent): void {
    if (this._moving) {
      if (e.touches[0].clientY < this.yStartPosition) {
        if (e.touches[0].clientY > 90 * window.innerHeight / 100) {
          let distance = this.yStartPosition - e.touches[0].clientY;
          this.el.nativeElement.style
            .setProperty('--ft-slideup-translate-y-on-moving', `${-distance}px`)
        }
      } else {
        let distance = e.touches[0].clientY - this.yStartPosition;
        this.el.nativeElement.style
          .setProperty('--ft-slideup-translate-y-on-moving', `${distance}px`)
      }
    }
    //this.actualizeCarouselTranslateOnMove(this.getXPositionOfTouchEvent(e.touches[0].clientX));
  }

  @HostListener('window:touchend', ['$event'])
  onWindowTouchend(e: TouchEvent): void {
    if (this._moving) {
      if (e.changedTouches[0].pageY > window.innerHeight / 2) {
        this._open = false;
        setTimeout(() => {
          this._renderer.removeClass(document.body, 'body--slideup-open');
          this._showOverlay = false;
          this.cd.detectChanges();
        }, 500);
      }
      this._moving = false;
      this.el.nativeElement.style
        .setProperty('--ft-slideup-translate-y-on-moving', `0px`)
    }
  }

  onSliderMousedown(event: MouseEvent): void {
    event.stopPropagation();
    this._moving = true;
    this.yStartPosition = event.y;
  }

  @HostListener('window:mousemove', ['$event'])
  onWindowMouseMove(e: MouseEvent): void {
    if (this._moving) {
      if (e.y < this.yStartPosition) {
        if (e.y > 90 * window.innerHeight / 100) {
          let distance = this.yStartPosition - e.y;
          this.el.nativeElement.style
            .setProperty('--ft-slideup-translate-y-on-moving', `${-distance}px`)
        }
      } else {
        let distance = e.y - this.yStartPosition;
        this.el.nativeElement.style
          .setProperty('--ft-slideup-translate-y-on-moving', `${distance}px`)
      }
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onWindowMouseUp(e: MouseEvent): void {
    if (this._moving) {
      if (e.y > window.innerHeight / 2) {
        this._open = false;
        setTimeout(() => {
          this._renderer.removeClass(document.body, 'body--slideup-open');
          this._showOverlay = false;
          this.cd.detectChanges();
        }, 500);
      }
      this._moving = false;
      this.el.nativeElement.style
        .setProperty('--ft-slideup-translate-y-on-moving', `0px`)
    }
  }

  changeOpenState(): void {
    this._open = !this._open;
    if (this._open) {
      this._showOverlay = true;
      this._renderer.addClass(document.body, 'body--slideup-open');
      this.cd.detectChanges();
    } else {
      setTimeout(() => {
        this._showOverlay = false;
        this._renderer.removeClass(document.body, 'body--slideup-open');
        this.cd.detectChanges();
      }, 500);
    }
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this._renderer.removeClass(document.body, 'body--slideup-open');
    this.cd.detectChanges();
  }

}
