import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideupComponent } from './slideup.component';

describe('SlideupComponent', () => {
  let component: SlideupComponent;
  let fixture: ComponentFixture<SlideupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [SlideupComponent]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
