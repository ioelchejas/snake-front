import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'sk-skeleton',
    templateUrl: './skeleton.component.html',
    styleUrls: ['./skeleton.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class SkeletonComponent {
  _height: number = 100;
  @Input() set height(value: number) {
    this._height = value;
  }

}
