import { ChangeDetectionStrategy, Component, ElementRef, forwardRef, HostListener, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import { NgClass, NgIf } from '@angular/common';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

@Component({
    selector: 'sk-text-field',
    templateUrl: './text-field.component.html',
    styleUrls: ['./text-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextFieldComponent),
            multi: true,
        },
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective, NgClass, FormsModule, NgIf]
})
export class TextFieldComponent implements ControlValueAccessor {
  _value: string = '';
  _content = false;
  _placeholder: string = '';
  _disabled: boolean = false;
  _required: boolean = false;
  _focused: boolean = false;
  _closeClass = '-slash';
  @Input() set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input() set disabled(value: boolean) {
    this._disabled = value;
  }

  @Input() set required(value: boolean) {
    this._required = value;
  }

  _passField: boolean = false;
  _type: 'text' | 'password' | 'email' | 'number' = 'text';
  @Input() set type(value: 'text' | 'password') {
    this._type = value;
    if (this._type === 'password') {
      this._passField = true;
    }
  }

  @ViewChild('input') inputRef: ElementRef;


  @HostListener('click')
  onClick(): void {
    if (!this._disabled) {
      this._focused = true;
      this.inputRef.nativeElement.focus();
    }
  }

  onBlur() {
    this.onTouched();
    if (this.inputRef.nativeElement.value.length > 0) {
      this._content = true;
    } else {
      this._content = false;
    }
    this._focused = false;
  }

  onFocus() {
    this._content = true;
    this._focused = true;
  }

  private onTouched = () => { };
  private onChange = (_: any) => { };

  writeValue(value: string): void {
    this._value = value;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

  onSetValue(value: string) {
    this.onChange(value);
  }

  onClickEye(e: MouseEvent): void {
    e.stopPropagation();
    if (!this._disabled) {
      this._closeClass = this._closeClass === '-slash' ? '' : '-slash';
      this._type = this._closeClass === '-slash' ? 'password' : 'text';
    }
  }

}
