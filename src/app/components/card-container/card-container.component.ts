import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'sk-card-container',
    templateUrl: './card-container.component.html',
    styleUrls: ['./card-container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class CardContainerComponent {
  _height: number;
  @Input() set height(value: number) {
    this._height = value;
  }

}
