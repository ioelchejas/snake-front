import { ChangeDetectionStrategy, Component, ElementRef, ViewChild } from '@angular/core';
import { AppContainerService } from '../../services/app-container/app-container.service';
import { NgTemplateOutlet } from '@angular/common';

@Component({
    selector: 'sk-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgTemplateOutlet]
})
export class HeaderComponent {
  @ViewChild('header') headerRef: ElementRef;

  constructor(private appContainerService: AppContainerService) { }

  ngAfterViewInit(): void {
    this.appContainerService.$paddingTop.next(this.headerRef.nativeElement.offsetHeight)
  }

}
