import { Component } from '@angular/core';
import { SelectDirective } from '../select.directive';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  standalone: true,
  hostDirectives: [SelectDirective]
})
export class SelectComponent {

}
