import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'sk-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class SpinnerComponent {
}
