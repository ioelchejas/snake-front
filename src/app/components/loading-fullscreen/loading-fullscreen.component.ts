import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Renderer2, ViewEncapsulation } from '@angular/core';
import { LoadingFullscreenService } from './loading-fullscreen.service';
import { TitleComponent } from '../title/title.component';
import { NgClass } from '@angular/common';

@Component({
    selector: 'sk-loading-fullscreen',
    templateUrl: './loading-fullscreen.component.html',
    styleUrls: ['./loading-fullscreen.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass, TitleComponent]
})
export class LoadingFullscreenComponent {
  _open: boolean = false;
  _title: string = ''
  constructor(private loadingFullscreenService: LoadingFullscreenService, private cd: ChangeDetectorRef, private _renderer: Renderer2) {
    this.loadingFullscreenService.$showLoading.subscribe((value: string) => {
      this._renderer.addClass(document.body, 'body--loading-fullscreen-open');
      this._open = true;
      this._title = value;
      this.cd.detectChanges();
    });
    this.loadingFullscreenService.$hideLoading.subscribe(() => {
      this._renderer.removeClass(document.body, 'body--loading-fullscreen-open');
      this._open = false;
      this.cd.detectChanges();
    });
  }

}
