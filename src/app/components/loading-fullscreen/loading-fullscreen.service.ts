import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingFullscreenService {
  $showLoading = new Subject<string>();
  $hideLoading = new Subject<void>();
  constructor() { }
}
