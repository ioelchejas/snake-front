import { TestBed } from '@angular/core/testing';

import { LoadingFullscreenService } from './loading-fullscreen.service';

describe('LoadingFullscreenService', () => {
  let service: LoadingFullscreenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingFullscreenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
