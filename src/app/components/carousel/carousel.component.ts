import { AfterViewInit, ChangeDetectionStrategy, Component, ContentChildren, ElementRef, EventEmitter, HostListener, Output, QueryList, ChangeDetectorRef } from '@angular/core';
import { CarouselItemComponent } from '../carousel-item/carousel-item.component';
import { NgClass } from '@angular/common';

@Component({
    selector: 'sk-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass]
})
export class CarouselComponent implements AfterViewInit {
  @ContentChildren(CarouselItemComponent, { descendants: false })
  tabQueryList!: QueryList<CarouselItemComponent>;

  @Output() changeSlide: EventEmitter<number> = new EventEmitter<number>();

  _moving = false;
  xStartPosition;
  @HostListener('mousedown', ['$event'])
  onMouseDown(e: MouseEvent): void {
    this._moving = true;
    this.xStartPosition = e.x;
    this.cd.detectChanges();
  }

  @HostListener('touchstart', ['$event'])
  onTouchStart(e: TouchEvent): void {
    this._moving = true;
    this.xStartPosition = e.touches[0].clientX;
    this.cd.detectChanges();
  }

  @HostListener('window:mousemove', ['$event'])
  onWindowMouseMove(e: MouseEvent): void {
    e.preventDefault();
    if (this._moving) {
      this._maxHeight = null;
      if (e.x < this.xStartPosition) { // restar transform
        let distance = this.xStartPosition - e.x;
        this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth - distance}px)`;
      } else {
        let distance = e.x - this.xStartPosition;
        this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth + distance}px)`;
      }
      this.cd.detectChanges();
    }
  }

  touchMovePostion;
  @HostListener('window:touchmove', ['$event'])
  onWindowTouchmove(e: TouchEvent): void {
    if (this._moving) {
      this._maxHeight = null;
      this.touchMovePostion = e.touches[0].clientX;
      if (e.touches[0].clientX < this.xStartPosition) { // restar transform
        let distance = this.xStartPosition - e.touches[0].clientX;
        this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth - distance}px)`;
      } else {
        let distance = e.touches[0].clientX - this.xStartPosition;
        this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth + distance}px)`;
      }
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onWindowMouseUp(e: MouseEvent): void {
    if (e.x === this.xStartPosition) {
      this._moving = false;
      this.cd.detectChanges();
    }
    if (this._moving) {
      setTimeout(() => {
        this._moving = false;
        if (e.x < this.xStartPosition && this.xStartPosition - e.x > this.el.nativeElement.offsetWidth / 2 && this.currentSlide < this.tabQueryList.toArray().length - 1) {
          this.goNext();
        } else if (e.x > this.xStartPosition && e.x - this.xStartPosition > this.el.nativeElement.offsetWidth / 2 && this.currentSlide > 0) {
          this.goPrev();
        } else {
          this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth}px)`;
          setTimeout(() => {
            this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
          }, 500);
        }
        this.cd.detectChanges();
      });
    }
  }

  @HostListener('window:touchend', ['$event'])
  onWindowTouchend(e: TouchEvent): void {
    if (this.touchMovePostion === this.xStartPosition) {
      this._moving = false;
      this.cd.detectChanges();
    }
    if (this._moving) {
      setTimeout(() => {
        this._moving = false;
        if (this.touchMovePostion < this.xStartPosition && this.xStartPosition - this.touchMovePostion > this.el.nativeElement.offsetWidth / 2 && this.currentSlide < this.tabQueryList.toArray().length - 1) {
          this.goNext();
        } else if (this.touchMovePostion > this.xStartPosition && this.touchMovePostion - this.xStartPosition > this.el.nativeElement.offsetWidth / 2 && this.currentSlide > 0) {
          this.goPrev();
        } else {
          this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth}px)`;
          setTimeout(() => {
            this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
            this.cd.detectChanges();
          }, 500);
        }
        this.cd.detectChanges();
      });
    }
  }

  //@HostListener('scroll')
  onScroll(): void {

  }

  currentSlide = 0;
  _transform = "translateX(0px)";

  _maxHeight = null;
  constructor(private el: ElementRef, private cd: ChangeDetectorRef) { }
  ngAfterViewInit(): void {
    //this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
  }

  goPrev(): void {
    if (this.currentSlide > 0 && !this._moving) {
      this.currentSlide--;
      this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth}px)`;
      setTimeout(() => {
        this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
        this.cd.detectChanges();
      }, 500);
      this.changeSlide.emit(this.currentSlide);
      this.cd.detectChanges();
    }
  }

  goNext(): void {
    if (this.currentSlide < this.tabQueryList.toArray().length - 1 && !this._moving) {
      this.currentSlide++;
      this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth}px)`;
      setTimeout(() => {
        this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
        this.cd.detectChanges();
      }, 500);
      this.changeSlide.emit(this.currentSlide);
      this.cd.detectChanges();
    }
  }

  goTo(index: number): void {
    this.currentSlide = index;
    this._transform = `translateX(${-this.currentSlide * this.el.nativeElement.offsetWidth}px)`;
    setTimeout(() => {
      this._maxHeight = this.tabQueryList.toArray()[this.currentSlide].el.nativeElement.offsetHeight;
      this.cd.detectChanges();
    }, 500);
    this.changeSlide.emit(this.currentSlide);
  }

}
