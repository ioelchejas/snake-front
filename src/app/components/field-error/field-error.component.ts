import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { NgIf } from '@angular/common';
@Component({
    selector: 'sk-field-error',
    templateUrl: './field-error.component.html',
    styleUrls: ['./field-error.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgIf]
})
export class FieldErrorComponent implements OnDestroy {

  @Input() errors: any;
  @Input() set form(formControl: UntypedFormControl) {

    if (this._form) {
      this.ngOnDestroy();
    }
    this._form = formControl;

    if (this._form) {
      this.subscriptions.add(
        this._form.statusChanges
          .pipe(
            map(() => this._form.errors),
            distinctUntilChanged()
          )
          .subscribe(() => {
            this.check();
          })
      );
    }
    this.check();
  }
  errorMessage!: string;

  _touched = false;

  set touched(value: boolean) {
    this._touched = value;
    this.cd.markForCheck();
  }

  private subscriptions = new Subscription();
  private _form!: UntypedFormControl;

  constructor(private cd: ChangeDetectorRef) { }

  private check(): void {
    let errorMessage = '';
    if (this.errors && this._form && this._form.errors) {
      const errorKeys = Object.keys(this.errors);
      for (const errorKey of errorKeys) {
        if (this._form.hasError(errorKey)) {
          errorMessage = this.errors[errorKey];
          break;
        }
      }
    }
    if (this.errorMessage !== errorMessage) {
      this.errorMessage = errorMessage;
      this.cd.markForCheck();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
