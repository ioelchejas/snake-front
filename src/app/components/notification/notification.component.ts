import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NotificationService } from './notification.service';
import { TitleComponent } from '../title/title.component';
import { NgClass } from '@angular/common';


const notificationTypes = {
  info: ['sk-notification--info'],
  alert: ['sk-notification--alert'],
  success: ['sk-notification--success'],
  error: ['sk-notification--error']
};

const notificationDurations = {
  small: 3000,
  medium: 5000,
  large: 10000
}

export interface notificationI {
  type: keyof typeof notificationTypes,
  duration: keyof typeof notificationDurations,
  title: string
}

@Component({
    selector: 'sk-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass, TitleComponent]
})
export class NotificationComponent {
  _open: boolean = false;
  _type: string = '';
  _title: string = ''
  constructor(private notificationService: NotificationService, private cd: ChangeDetectorRef) {
    this.notificationService.$showNotification.subscribe((value: notificationI) => {
      this._open = true;
      this._type = (notificationTypes[value.type] || []).join(' ');
      this._title = value.title;
      this.cd.detectChanges();
      setTimeout(() => {
        this._open = false;
        this.cd.detectChanges();
      }, notificationDurations[value.duration]);
    });
  }

}
