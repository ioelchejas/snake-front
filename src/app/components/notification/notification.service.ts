import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { notificationI } from './notification.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  $showNotification = new Subject<notificationI>();
  constructor() { }
}
