import { Component, Input } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
    selector: 'sk-button-icon-container',
    templateUrl: './button-icon-container.component.html',
    styleUrls: ['./button-icon-container.component.scss'],
    standalone: true,
    imports: [NgClass]
})
export class ButtonIconContainerComponent {
  _bottom = false;
  @Input() set bottom(value: boolean) {
    this._bottom = value;
  }

}
