import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonIconContainerComponent } from './button-icon-container.component';

describe('ButtonIconContainerComponent', () => {
  let component: ButtonIconContainerComponent;
  let fixture: ComponentFixture<ButtonIconContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [ButtonIconContainerComponent]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonIconContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
