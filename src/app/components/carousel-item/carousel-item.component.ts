import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, NgZone,  ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'sk-carousel-item',
    templateUrl: './carousel-item.component.html',
    styleUrls: ['./carousel-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class CarouselItemComponent implements AfterViewInit {
  _height: number;
  onStableSubscription: Subscription;
  constructor(public el: ElementRef, private ngZone: NgZone, private cd: ChangeDetectorRef) { }
  ngAfterViewInit(): void {
    this.onStableSubscription = this.ngZone.onStable.subscribe(() => {
      this._height = window.innerHeight - this.el.nativeElement.getBoundingClientRect().y - 92;
      if (this._height < 0) {
        this._height = window.innerHeight * .9 - 92 - 70 - 40 - 16;
      }
      this.cd.detectChanges();
      this.onStableSubscription.unsubscribe();
    })
  }

}
