import { AfterContentInit, AfterViewInit, ChangeDetectionStrategy, Component, ContentChild, ContentChildren, forwardRef, OnInit, QueryList } from '@angular/core';
import { UntypedFormControl, FormControlName } from '@angular/forms';
import { FieldErrorComponent } from '../field-error/field-error.component';

@Component({
    selector: 'sk-form-field',
    templateUrl: './form-field.component.html',
    styleUrls: ['./form-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class FormFieldComponent implements AfterContentInit {
  @ContentChildren(FormControlName) formControlList!: QueryList<FormControlName>;
  @ContentChild(forwardRef(() => FieldErrorComponent)) error!: FieldErrorComponent;
  constructor() { }
  private formControl!: UntypedFormControl;
  ngAfterContentInit(): void {
    this.formControl = this.formControlList.toArray()[0].control;
    if (this.error) {
      this.error.form = this.formControl;
    }
  }

  private touched = false;
  ngDoCheck(): void {
    if (this.formControlList && this.formControl.touched !== this.touched) {
      this.touched = this.formControl.touched;
      if (this.error) {
        this.error.touched = this.formControl.touched;
      }
    }
  }

}
