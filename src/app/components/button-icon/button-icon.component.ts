import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgStyle, NgClass } from '@angular/common';
import { RippleDirective } from '../../directives/ripple/ripple.directive';

@Component({
    selector: 'sk-button-icon',
    templateUrl: './button-icon.component.html',
    styleUrls: ['./button-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [RippleDirective, NgStyle, NgClass]
})
export class ButtonIconComponent implements OnInit {
  _backgroundColor = '';
  constructor() { }
  @Input() set backgroundColor(value: string) {
    this._backgroundColor = value;
  }

  _disabled = false;
  @Input() set disabled(value: boolean) {
    this._disabled = value;
  }

  @Output() btnClick: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }

  onClick(): void {
    if (!this._disabled) {
      this.btnClick.emit();
    }
  }

}
