import { ChangeDetectionStrategy, Component, Input, OnDestroy, Renderer2, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { NgClass, NgIf } from '@angular/common';

@Component({
    selector: 'sk-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass, NgIf]
})
export class ModalComponent implements OnDestroy {
  _open: boolean = false;
  @Input() set open(value: boolean) {
    this._open = value;
  }
  constructor(private _renderer: Renderer2, private cd: ChangeDetectorRef) { }

  openModal(): void {
    this.open = true;
    this._renderer.addClass(document.body, 'body--modal-open');
    this.cd.detectChanges();
  }

  closeModal(): void {
    this.open = false;
    this._renderer.removeClass(document.body, 'body--modal-open');
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this._renderer.removeClass(document.body, 'body--modal-open');
    this.cd.detectChanges();
  }

}
