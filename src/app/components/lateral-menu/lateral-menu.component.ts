import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Input, OnDestroy, Renderer2, ViewEncapsulation } from '@angular/core';
import { ButtonIconComponent } from '../button-icon/button-icon.component';
import { TitleComponent } from '../title/title.component';
import { NgClass, NgIf } from '@angular/common';

@Component({
    selector: 'sk-lateral-menu',
    templateUrl: './lateral-menu.component.html',
    styleUrls: ['./lateral-menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
    imports: [NgClass, TitleComponent, ButtonIconComponent, NgIf]
})
export class LateralMenuComponent implements OnDestroy {
  _open = false;
  _canClose = false;

  @Input() set open(value: boolean) {
    this._open = value;
    this._canClose = false;
  }

  @HostListener('document:click')
  onDocumentClick(): void {
    if (!this.wasInside && this._canClose) {
      this.open = false;
      this.cd.detectChanges();
    }
    if(this._open){
      this._canClose = true;
    }
    this.wasInside = false;
  }

  wasInside = false;
  @HostListener('click')
  onClickInside(): void {
    this.wasInside = true;
  }

  constructor(private cd: ChangeDetectorRef, private _renderer: Renderer2) { }

  openMenu(): void {
    this._open = true;
    this._canClose = false;
    this._renderer.addClass(document.body, 'body--lateral-menu-open');
  }

  onClickOverlay(): void {
    this._open = false;
    this._canClose = false;
    this._renderer.removeClass(document.body, 'body--lateral-menu-open');

  }

  closeMenu(): void {
    this._open = false;
    this._canClose = false;
    this._renderer.removeClass(document.body, 'body--lateral-menu-open');
  }

  ngOnDestroy(): void {
    this._renderer.removeClass(document.body, 'body--lateral-menu-open');
  }

}
