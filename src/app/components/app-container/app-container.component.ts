import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AppContainerService } from '../../services/app-container/app-container.service';

@Component({
    selector: 'sk-app-container',
    templateUrl: './app-container.component.html',
    styleUrls: ['./app-container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true
})
export class AppContainerComponent implements OnInit {
  paddingBottom: number = 0;
  paddingTop: number = 0;
  constructor(private appContainerService: AppContainerService, private cd: ChangeDetectorRef) {
    this.appContainerService.$paddingBottom.subscribe((value)=>{
      this.paddingBottom = value;
      this.cd.detectChanges();
    })
    this.appContainerService.$paddingTop.subscribe((value)=>{
      this.paddingTop = value;
      this.cd.detectChanges();
    })
   }

  ngOnInit(): void {
  }

}
