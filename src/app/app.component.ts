import { Component } from '@angular/core';
import { NotificationComponent } from './components/notification/notification.component';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    standalone: true,
    imports: [RouterOutlet, NotificationComponent]
})
export class AppComponent {
  title = 'snake-front';
}
