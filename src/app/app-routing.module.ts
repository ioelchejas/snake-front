import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { LayoutComponent } from './layouts/layout/layout.component';
import { ChatComponent } from './pages/chat/chat.component';
import { LoginComponent } from './pages/login/login.component';
import { SnakeGameComponent } from './pages/snake-game/snake-game.component';
import { RegisterComponent } from './pages/register/register.component';
import { SnakeOnlineGameComponent } from './pages/snake-online-game/snake-online-game.component';
// import { WidgetDemoComponent } from 'src/demo/widget-demo/widget-demo.component';

const pages: Routes = [
  { path: 'snake-game', component: SnakeGameComponent },
  { path: 'snake-online-game', component: SnakeOnlineGameComponent },
  { path: 'chat', component: ChatComponent },
]

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'sign-in', component: RegisterComponent },
  // { path: 'demo', component: WidgetDemoComponent },
  { path: 'app', component: LayoutComponent, children: pages, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking',
    useHash: false,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

