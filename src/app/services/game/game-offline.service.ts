import { Injectable } from '@angular/core';
import { GameService } from './game.service';

@Injectable()
export class OfflineGameService extends GameService {
    INITIAL_SNAKE_LENGTH: number = 4;
    GAME_COLUMNS: number = 15;
    GAME_ROWS: number = 15;
    subscribeToInterval(): void {
        this.subscriptions.add(
            this.interval$.subscribe(({ direction }) => {
                this.canChangeDirection = true;

                this.handleMove(
                    this.isOutOfLimit(direction, this.snakes[0].cells),
                    this.getCellToMove(direction, this.snakes[0].cells),
                    direction,
                    this.snakes[0].cells
                );
            })
        );
    }
}
