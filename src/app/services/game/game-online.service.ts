import { Injectable } from '@angular/core';
import { OnlineEventRequestType } from 'src/app/interfaces/online-game-events';
import { GameService } from './game.service';
import { SnakeOnlineGameService } from '../../pages/snake-online-game/services/snake-online-game.service';
import { Direction } from 'src/app/interfaces/game.interface';

@Injectable()
export class OnlineGameService extends GameService {
    INITIAL_SNAKE_LENGTH: number = 4;
    GAME_COLUMNS: number = 25;
    GAME_ROWS: number = 25;

    constructor(private snakeOnlineGameService: SnakeOnlineGameService) {
        super();

        this.subscriptions.add(
            this.snakeOnlineGameService.startGame$.subscribe(event => {
                this.startGame(event);
            })
        );

        this.subscriptions.add(
            this.snakeOnlineGameService.movementEvent$.subscribe(event => {

                let movementDirection: Direction;

                switch (event.gameEvent) {
                    case 'DOWN':
                        movementDirection = 'ArrowDown';
                        break;
                    case 'UP':
                        movementDirection = 'ArrowUp';
                        break;
                    case 'RIGHT':
                        movementDirection = 'ArrowRight';
                        break;
                    case 'LEFT':
                        movementDirection = 'ArrowLeft';
                        break;
                }

                this.handleMove(
                    this.isOutOfLimit(movementDirection, this.snakes.find(s => s.snakeId === event.userId).cells),
                    this.getCellToMove(movementDirection, this.snakes.find(s => s.snakeId === event.userId).cells),
                    movementDirection,
                    this.snakes.find(s => s.snakeId === event.userId).cells
                );
            })
        )
    }

    subscribeToInterval(): void {
        this.subscriptions.add(
            this.interval$.subscribe(({ direction }) => {
                this.canChangeDirection = true;
                let gameEvent: OnlineEventRequestType;
                switch (direction) {
                    case 'ArrowDown':
                        gameEvent = 'DOWN';
                        break;
                    case 'ArrowUp':
                        gameEvent = 'UP';
                        break;
                    case 'ArrowRight':
                        gameEvent = 'RIGHT';
                        break;
                    case 'ArrowLeft':
                        gameEvent = 'LEFT';
                        break;
                }
                this.snakeOnlineGameService.sendOnlineEvent(gameEvent);
            })
        );
    }

}
