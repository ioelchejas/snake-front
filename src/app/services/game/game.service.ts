import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, Subject, Subscription, merge, fromEvent } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';
import { CellI, Direction, SnakeI } from '../../interfaces/game.interface';

@Injectable()
export abstract class GameService {
    abstract GAME_COLUMNS: number;
    abstract GAME_ROWS: number;
    abstract INITIAL_SNAKE_LENGTH: number;

    protected _cells$ = new BehaviorSubject<CellI[]>([]);
    cells$ = this._cells$.asObservable();
    private _score$ = new BehaviorSubject<number>(0);
    score$ = this._score$.asObservable();
    private _playing$ = new BehaviorSubject<boolean>(false);
    playing$ = this._playing$.asObservable();

    private pause = false;
    private direction: Direction = 'ArrowRight';
    private mobileKeys$ = new Subject<KeyboardEvent>();
    private lostGame$ = new Subject<void>();
    private footCell: number;

    protected snakes: SnakeI[] = [];
    protected subscriptions = new Subscription();
    protected interval$ = interval(200).pipe(
        takeUntil(this.lostGame$),
        map(() => ({
            direction: this.direction
        })),
    ).pipe(
        takeUntil(this.lostGame$),
        tap(() => console.log("pause: ", this.pause)),
        filter(() => !this.pause)
    );

    constructor() {
        this.subscriptions.add(this.lostGame$.subscribe(() => {
            this._playing$.next(false);
        }));
    }

    startGame(snakesId: string[]): void {
        this.resetGame();
        snakesId.forEach(snakeId => {
            this.generateSnake(snakeId);
        })
        this.generateFoot();
        this.subscribeToInterval();
        this.subscribeToKeys();
        this._playing$.next(true);
    }

    private resetGame(): void {
        // mover a la subscripcion ? reducer ???
        this.canChangeDirection = true;

        // cambiar por estrategia para acomodar snakes
        this.snakesGenerated = 0;

        this._score$.next(0);
        this.direction = 'ArrowRight';
        this.resetCellsState();
        this.clearSnakes();
        this.clearFoot();
    }


    // GAME ELEMENTS INITIALIZATION

    private resetCellsState(): void {
        const cells: CellI[] = [];
        for (let i = 0; i < this.GAME_COLUMNS * this.GAME_ROWS; i++) {
            cells.push({
                isFood: false,
                isSnake: false,
                snakeFirst: '',
                snakeLast: '',
                snakeDirection: '',
                snakeId: undefined
            });
        }
        this._cells$.next(cells);
    }

    private clearSnakes(): void {
        this.snakes = []
    }

    private clearFoot(): void {
        this.footCell = undefined;
    }

    snakesGenerated = 0;
    private generateSnake(snakeId: string): void {
        const cells = this._cells$.getValue();
        let initSnakePosition: number = this.snakesGenerated === 0 ? 0 : 210;
        this.snakes.push({
            snakeId: snakeId,
            cells: [initSnakePosition]
        });
        cells[initSnakePosition].isSnake = true;
        cells[initSnakePosition].snakeId = snakeId;
        for (let i = 1; i < this.INITIAL_SNAKE_LENGTH; i++) {
            cells[initSnakePosition + i].isSnake = true;
            cells[initSnakePosition + i].snakeDirection = `sk-snake-game__cell--snake--${this.direction.toLowerCase().substring(5)}`;
            this.snakes.find(s => s.snakeId === snakeId).cells.push(initSnakePosition + i);
        }
        cells[this.snakes.find(s => s.snakeId === snakeId).cells[0]].snakeLast = `sk-snake-game__cell--snake-last`;
        cells[this.snakes.find(s => s.snakeId === snakeId).cells[this.INITIAL_SNAKE_LENGTH - 1]].snakeFirst = `sk-snake-game__cell--snake-first`;
        this._cells$.next(cells);
        this.snakesGenerated++;
    }

    private generateFoot(): void {
        const cells = this._cells$.getValue();
        let excludedCells: number[] = [];
        this.snakes.forEach(snake => {
            excludedCells = [...excludedCells, ...snake.cells];
        });
        let initFoodPosition: number = this.getRandomNumber(0, this.GAME_COLUMNS * this.GAME_ROWS - 1, excludedCells);
        this.footCell = initFoodPosition;
        cells[initFoodPosition].isFood = true;
        this._cells$.next(cells);
    }

    private getRandomNumber(start: number, end: number, excluded?: number[]): number {
        let n: number;
        while (n === undefined || (excluded && excluded.includes(n))) {
            n = Math.floor((Math.random() * (end - start + 1) + start));
        }
        return n;
    }


    // HANDLE GAME EVENTS

    protected isOutOfLimit(direction: Direction, snakeCells: number[]): boolean {
        let row: number = Math.trunc(snakeCells[snakeCells.length - 1] / (this.GAME_COLUMNS));
        switch (direction) {
            case 'ArrowUp':
                return row === 0;
            case 'ArrowDown':
                return row === this.GAME_ROWS - 1;
            case 'ArrowLeft':
                let rowOfBeforeCell = Math.trunc((snakeCells[snakeCells.length - 1] - 1) / (this.GAME_COLUMNS));
                return rowOfBeforeCell < row;
            case 'ArrowRight':
                let rowOfNextCell = Math.trunc((snakeCells[snakeCells.length - 1] + 1) / (this.GAME_COLUMNS));
                return rowOfNextCell > row;
        }
    }

    protected getCellToMove(direction: Direction, snakeCells: number[]): number {
        switch (direction) {
            case 'ArrowUp':
                return snakeCells[snakeCells.length - 1] - this.GAME_COLUMNS;
            case 'ArrowDown':
                return snakeCells[snakeCells.length - 1] + this.GAME_COLUMNS;
            case 'ArrowLeft':
                return snakeCells[snakeCells.length - 1] - 1;
            case 'ArrowRight':
                return snakeCells[snakeCells.length - 1] + 1;
        }
    }

    private lastFood = false;
    protected handleMove(isOutOfLimit: boolean, toPosition: number, direction: Direction, snakeCells: number[]): void {
        const cells = this._cells$.getValue();

        let allSnakeCells: number[] = []
        this.snakes.forEach(snake => {
            allSnakeCells = [...allSnakeCells, ...snake.cells];
        });

        if (isOutOfLimit || allSnakeCells.includes(toPosition)) {
            this.lostGame$.next(); // EMITIR AL WS LOOSE EVENT
        } else if (toPosition === this.footCell) {
            cells[toPosition] = {
                isFood: false,
                isSnake: true,
                snakeDirection: `sk-snake-game__cell--snake--${direction.toLowerCase().substring(5)}`,
                snakeFirst: `sk-snake-game__cell--snake-first`,
                snakeLast: '',
            }
            snakeCells.push(toPosition);

            this._score$.next(this._score$.value + 1);
            this.generateFoot();
            this.lastFood = true;
        } else {
            // delete current head
            cells[snakeCells[snakeCells.length - 1]].snakeFirst = '';

            // new head
            cells[toPosition].snakeFirst = `sk-snake-game__cell--snake-first`;
            cells[toPosition].isSnake = true;
            cells[toPosition].snakeDirection = `sk-snake-game__cell--snake--${direction.toLowerCase().substring(5)}`;
            snakeCells.push(toPosition);


            //  delete current final
            cells[snakeCells[0]].isSnake = false;
            cells[snakeCells[0]].snakeDirection = '';
            cells[snakeCells[0]].snakeLast = '';
            snakeCells.shift();

            // new last
            cells[snakeCells[0]].snakeLast = `sk-snake-game__cell--snake-last`;

        }
        if (this.lastFood) {
            cells[snakeCells[snakeCells.length - 2]].snakeFirst = ``;
            this.lastFood = false;
        }
        this._cells$.next(cells);
    }

    canChangeDirection = true;

    private subscribeToKeys(): void {
        this.subscriptions.add(
            merge(fromEvent(window, 'keydown'), this.mobileKeys$)
                .pipe(
                    takeUntil(this.lostGame$),
                    filter((ev: KeyboardEvent) =>
                        this.canChangeDirection && !this.pause && (
                            (ev.key === 'ArrowUp' && this.direction !== 'ArrowDown' && this.direction !== 'ArrowUp')
                            || (ev.key === 'ArrowDown' && this.direction !== 'ArrowDown' && this.direction !== 'ArrowUp')
                            || (ev.key === 'ArrowLeft' && this.direction !== 'ArrowLeft' && this.direction !== 'ArrowRight')
                            || (ev.key === 'ArrowRight' && this.direction !== 'ArrowLeft' && this.direction !== 'ArrowRight'))
                    ),
                    map((ev: KeyboardEvent) => ({
                        direction: ev.key,
                    })),
                )
                .subscribe(({ direction }) => {
                    this.canChangeDirection = false;
                    this.direction = direction as Direction;
                })
        );
    }

    abstract subscribeToInterval(): void;

    pauseResume(): void {
        this.pause = !this.pause;
    }

    mobileControl(ev: KeyboardEvent): void {
        this.mobileKeys$.next(ev);
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }

}
