import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppContainerService {

  $paddingBottom = new Subject<number>();
  $paddingTop = new Subject<number>();
  constructor() {  }
}
