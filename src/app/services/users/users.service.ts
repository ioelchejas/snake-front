import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CookieService } from '../cookie/cookie.service';
import { RankingI } from './interfaces/ranking.interface';
import { UpdateScoreI } from './interfaces/update-score.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private rankingResponseSubject: Subject<RankingI[]> = new Subject();
  rankingResponse$ = this.rankingResponseSubject.asObservable();

  constructor(private _http: HttpClient, private cookieService: CookieService) { }

  setUserScore({ id, score }: UpdateScoreI): void {
    let url = 'users';
    const data = {
      id: id,
      score: score
    }
    this._http
      .put(
        url,
        data,
        {
          headers: new HttpHeaders()
            .set('Authorization', `${this.cookieService.getAccessToken()}`)
        }
      ).subscribe((res: string) => {
      }, (error: HttpErrorResponse) => {
      });
  }

  updateRanking(score: number): void {
    let url = 'ranking';
    const data = {
      score: score
    }
    this._http
      .post(
        url,
        data,
        {
          headers: new HttpHeaders()
            .set('Authorization', `${this.cookieService.getAccessToken()}`)
        }
      ).subscribe((res: string) => {
        this.getRanking();
      }, (error: HttpErrorResponse) => {
        this.getRanking();
      });
  }

  getRanking(): void {
    let url = 'ranking';
    this._http
      .get(
        url,
        {
          headers: new HttpHeaders()
            .set('Authorization', `${this.cookieService.getAccessToken()}`)
        }
      ).subscribe((res: RankingI[]) => {
        this.rankingResponseSubject.next(res);
      }, (error: HttpErrorResponse) => {
        console.log("error: ", error)
      });
  }
}
