export interface UpdateScoreI {
    id: string;
    score: number;
}