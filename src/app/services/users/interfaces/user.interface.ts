export interface UserI {
    _id:      string;
    name:     string;
    lastName: string;
    email:    string;
    password: string;
    roles:    string[];
    __v:      number;
    score:    number;
}
