export interface RankingI {
    _id:   string;
    email: string;
    score: number;
    __v:   number;
}
