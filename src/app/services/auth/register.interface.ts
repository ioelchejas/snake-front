export interface RegisterI {
    name: string;
    lastName: string;
    email: string;
    password: string;
}