import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from '../cookie/cookie.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LoginI } from './login.interface';
import { RegisterI } from './register.interface';
import { UserI } from '../users/interfaces/user.interface';
import { AppStateService } from '../app-state.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // SERVER ERROR

  private internalServerErrorSubject: Subject<void> = new Subject();
  internalServerError$ = this.internalServerErrorSubject.asObservable();


  // LOGIN

  private loginSuccessResponseSubject: Subject<string> = new Subject();
  loginSuccessResponse$ = this.loginSuccessResponseSubject.asObservable();

  private loginUnauthorizedResponseSubject: Subject<void> = new Subject();
  loginUnauthorizedResponse$ = this.loginUnauthorizedResponseSubject.asObservable();


  // REGISTER

  private registerSuccessResponseSubject: Subject<string> = new Subject();
  registerSuccessResponse$ = this.registerSuccessResponseSubject.asObservable();

  private registerConflictResponseSubject: Subject<void> = new Subject();
  registerConflictResponse$ = this.registerConflictResponseSubject.asObservable();


  // USER DATA

  private userDataSubject: Subject<UserI> = new Subject();
  userData$ = this.userDataSubject.asObservable();

  constructor(
    private appState: AppStateService,
    private _http: HttpClient,
    private cookieService: CookieService,
    private router: Router) {
    this.loginSuccessResponse$.subscribe((jwtToken: string) => {
      this.saveToken(jwtToken);
      this.getUserData(this.decodeToken(jwtToken));
    });
    this.registerSuccessResponse$.subscribe((jwtToken: string) => {
      this.saveToken(jwtToken);
      this.getUserData(this.decodeToken(jwtToken));
    });
  }

  private getUserData(id: string): void {
    let url = `users/${id}`;
    this._http
      .get(url, {
        headers: { 'Authorization': `${this.cookieService.getAccessToken()}` }
      }).subscribe((res: UserI) => {
        this.appState.setUserID(res._id);
        localStorage.setItem('user', JSON.stringify(res));
        this.userDataSubject.next(res);
      }, (error: HttpErrorResponse) => { });
  }

  login({ email, password }: LoginI): void {
    let url = 'auth/signin';
    const data = new HttpParams()
      .set('email', email)
      .set('password', password)
    this._http
      .post(
        url,
        data.toString(),
        {
          headers: new HttpHeaders()
            .set('content-type', 'application/x-www-form-urlencoded')
        }
      ).subscribe((res: string) => {
        this.loginSuccessResponseSubject.next(res);
      }, (error: HttpErrorResponse) => {
        console.log("status", error.status)
        switch (error.status) {
          case 401:
            this.loginUnauthorizedResponseSubject.next();
            break;
          case 503:
            this.internalServerErrorSubject.next();
            break;
          default:
            this.internalServerErrorSubject.next();
            break;
        }
      });
  }

  async authorize(): Promise<boolean> {
    if (this.checkCredentials()) {
      return true;
    } else {
      this.router.navigate(['/', 'login']);
      return false;
    }
  }


  // REGISTER

  register({ name, lastName, email, password }: RegisterI): void {
    let url = 'auth/signup';
    const data = new HttpParams()
      .set('name', name)
      .set('lastName', lastName)
      .set('email', email)
      .set('password', password);
    this._http
      .post(
        url,
        data.toString(),
        {
          headers: new HttpHeaders()
            .set('content-type', 'application/x-www-form-urlencoded')
        }
      ).subscribe((res: string) => {
        this.registerSuccessResponseSubject.next(res);
      }, (error: HttpErrorResponse) => {
        switch (error.status) {
          case 409:
            this.registerConflictResponseSubject.next();
            break;
          case 503:
            this.internalServerErrorSubject.next();
            break;
          default:
            this.internalServerErrorSubject.next();
        }
      });
  }

  // SAVE COOKIE DATA

  private saveToken(jwtToken: string) {
    this.cookieService.putAccessToken(jwtToken);
  }

  checkCredentials() {
    return this.cookieService.hasAccessToken();
  }

  // LOGOUT

  logout(): void {
    this.cookieService.removeSessionCookies();
    localStorage.removeItem('user');
  }

  // DECODE JWT TOKEN
  decodeToken(token: string): string {
    const _decodeToken = (token) => {
      try {
        return JSON.parse(atob(token));
      } catch {
        return;
      }
    };
    return token
      .split('.')
      .map(token => _decodeToken(token))
      .reduce((acc, curr) => {
        if (!!curr) acc = { ...acc, ...curr };
        return acc;
      }, Object.create(null)).id;
  }

}
