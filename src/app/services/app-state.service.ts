import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

interface AppState {
  userId: string;
}

@Injectable({
  providedIn: 'root'
})
export class AppStateService {
  private readonly LOCAL_STORAGE_STATE_KEY = 'app_state';

  private readonly initialState: AppState = {
    userId: undefined,
  };

  private _data$: BehaviorSubject<AppState> = new BehaviorSubject<AppState>(
    this.initialState
  );
  data$: Observable<AppState> = this._data$.asObservable();

  constructor() {
    this.hydrate();
    this.data$
      .pipe(filter((data) => !!data))
      .subscribe((newData: AppState) => this.setHydration(newData));
  }

  setUserID = (userId: string): void => {
    this._data$.next({
      ...this._data$.value,
      userId,
    });
  };

  reset = (): void => {
    this._data$.next(this.initialState);
  };

  private setState = (value: AppState): void => {
    this._data$.next(value);
  };

  private hydrate = () => {
    const storageValue = localStorage.getItem(this.LOCAL_STORAGE_STATE_KEY);
    if (storageValue) {
      try {
        const state = JSON.parse(storageValue);
        this.setState(state);
      } catch {
        localStorage.removeItem(this.LOCAL_STORAGE_STATE_KEY);
      }
    }
  };

  private setHydration = <T>(value: T) => {
    localStorage.setItem(this.LOCAL_STORAGE_STATE_KEY, JSON.stringify(value));
  };

}
