import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService as NgxCookieService } from 'ng2-cookies';

@Injectable({
  providedIn: 'root',
})
export class CookieService {

  private readonly ACCESS_TOKEN = 'access_token';

  constructor(private cookieService: NgxCookieService, private jwtHelper: JwtHelperService) { }

  removeSessionCookies(): void {
    console.log("remove")
    this.cookieService.set(this.ACCESS_TOKEN, '', -1);
  }

  getAccessToken(): string {
    return this.cookieService.get(this.ACCESS_TOKEN);
  }

  putAccessToken(accessToken: string): void {
    let expireDate = this.jwtHelper.getTokenExpirationDate(accessToken);
    this.cookieService.set(this.ACCESS_TOKEN, accessToken, new Date(expireDate));
  }

  hasAccessToken(): boolean {
    return this.cookieService.check(this.ACCESS_TOKEN);
  }
}
